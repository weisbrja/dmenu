# dmenu

This is my build of [dmenu](https://tools.suckless.org/dmenu).

## Patches

* [dmenu-xresources](https://tools.suckless.org/dmenu/patches/xresources/dmenu-xresources-4.9.diff)
* [dmenu-password](https://tools.suckless.org/dmenu/patches/password/dmenu-password-4.9.diff)
